#ifdef __APPLE__

#include <OpenGL/gl.h>

#else
#include <GL/gl.h>
#endif

#ifdef __APPLE__

#include <GLUT/glut.h>

#else
#include <GL/glut.h>
#endif

#include <cmath>

#define CAMERA_PERSPECTIVE 0
#define CAMERA_ORTHOGONAL  1
#define ORTHO_VOLUME_SIDE 20
#define PERSPEC_VOLUM_SIDE 3
#define FPS (1.0/60.0)

#define FLOOR_SIZE_SIDE 20
const int W_WIDTH = 500;
const int W_HEIGHT = 500;

int cam_type = CAMERA_PERSPECTIVE;

float cam_alpha = 0;
float cam_radius = 10;
float cam_height = 10;

int vpWidth;
int vpHeight;

void RenderScene()
{
    glPushMatrix();
        glColor3f(1, 1, 1);
        glBegin(GL_TRIANGLE_STRIP);
            glVertex3f( FLOOR_SIZE_SIDE, 0,  FLOOR_SIZE_SIDE);
            glVertex3f( FLOOR_SIZE_SIDE, 0, -FLOOR_SIZE_SIDE);
            glVertex3f(-FLOOR_SIZE_SIDE, 0,  FLOOR_SIZE_SIDE);
            glVertex3f(-FLOOR_SIZE_SIDE, 0, -FLOOR_SIZE_SIDE);
        glEnd();
    glPopMatrix();

    glPushMatrix();
    glColor3f(0, 1, 0);
    glRotatef(90, 0, 1, 0);
    glutSolidTeapot(1);
    glPopMatrix();

    glPushMatrix();
    glColor3f(1, 0, 0);
    glTranslatef(0, 0, 4);
    glRotatef(90, 0, 1, 0);
    glutWireCube(2);
    glPopMatrix();
}

void SetCamera()
{
    float camera_x = cosf(cam_alpha) * cam_radius;
    float camera_z = sinf(cam_alpha) * cam_radius;

    gluLookAt(camera_x, cam_height, camera_z,
              0, 0, 0,
              0, 1, 0);
}

void Display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    SetCamera();
    RenderScene();

    glutSwapBuffers();
}

void SetCameraVolum(int width, int height)
{
    glMatrixMode(GL_PROJECTION);

    if (CAMERA_PERSPECTIVE == cam_type)
        gluPerspective(60, (GLdouble) width / height, 1.0f, 100.0f);
    else if (CAMERA_ORTHOGONAL == cam_type)
        glOrtho(-ORTHO_VOLUME_SIDE / 2, ORTHO_VOLUME_SIDE / 2, -ORTHO_VOLUME_SIDE / 2, ORTHO_VOLUME_SIDE / 2, 1, 100);

    glMatrixMode(GL_MODELVIEW);
}

void SetViewportSize(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    glMatrixMode(GL_MODELVIEW);
}

void Reshape(int width, int height)
{
    SetViewportSize(width, height);
    SetCameraVolum(width, height);
    vpWidth = width;
    vpHeight = height;
}

void SpecialKeyboard(int key, int x, int y)
{

    switch (key)
    {
        case GLUT_KEY_UP:
            cam_height -= FPS * 10;
            cam_radius -= FPS * 10;
            break;
        case GLUT_KEY_DOWN:
            cam_height += FPS * 10;
            cam_radius += FPS * 10;
            break;
        case GLUT_KEY_RIGHT:
            cam_alpha += M_PI * FPS;
            break;
        case GLUT_KEY_LEFT:
            cam_alpha -= M_PI * FPS;
            break;
    }

    glutPostRedisplay();
}

void Keyboard(unsigned char key, int x, int y)
{
    if (key == 'p')
    {
        cam_type = (cam_type == CAMERA_ORTHOGONAL) ? CAMERA_PERSPECTIVE : CAMERA_ORTHOGONAL;
        Reshape(vpWidth, vpHeight);
    }

    glutPostRedisplay();
}

void WireEvents()
{
    glutKeyboardFunc(Keyboard);
    glutSpecialFunc(SpecialKeyboard);
    glutReshapeFunc(Reshape);
    glutDisplayFunc(Display);
}

void InitializeState()
{
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
}

void CreateWindow()
{
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(W_WIDTH, W_HEIGHT);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);

    glutCreateWindow("Mi primera Ventana");
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    CreateWindow();
    InitializeState();
    WireEvents();

    glutMainLoop();
    return 0;
}
