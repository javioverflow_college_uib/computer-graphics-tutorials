#ifdef __APPLE__

#include <OpenGL/gl.h>

#else
#include <GL/gl.h>
#endif

#ifdef __APPLE__

#include <GLUT/glut.h>

#else
#include <GL/glut.h>
#endif

#include <cmath>

#define CAMERA_PERSPECTIVE 0
#define CAMERA_ORTHOGONAL  1
#define ORTHO_VOLUME_SIDE 20
#define PERSPEC_VOLUM_SIDE 3
#define FPS (1.0/60.0)

#define FLOOR_SIZE_SIDE 20
const int W_WIDTH = 500;
const int W_HEIGHT = 500;

int cam_type = CAMERA_PERSPECTIVE;

float cam_alpha = 0;
float cam_radius = 10;
float cam_height = 10;

int vpWidth;
int vpHeight;

float light0_z = 0;
float light0_y = 2;
float light0_x = 0;
float light0_ambient = 0.2;
float light0_diffuse = 0.5;
float light0_specular = 0.5;

float ball_alpha = 0;
float ball_radius = 5;

void BallRotator(int timerIdentifier)
{
    glutTimerFunc(FPS * 1000, BallRotator, 0);
    ball_alpha = (float) fmod(ball_alpha + (FPS * M_PI), M_PI * 2);
    glutPostRedisplay();
}

void Box(GLfloat side_x, GLfloat side_y, GLfloat side_z)
{
    glBegin(GL_QUADS);
    glNormal3f(0, 0, -1);
    glVertex3f(-side_x / 2, -side_y / 2, 0);
    glVertex3f(-side_x / 2, side_y / 2, 0);
    glVertex3f(side_x / 2, side_y / 2, 0);
    glVertex3f(side_x / 2, -side_y / 2, 0);

    glNormal3f(0, 0, 1);
    glVertex3f(-side_x / 2, -side_y / 2, side_z);
    glVertex3f(-side_x / 2, side_y / 2, side_z);
    glVertex3f(side_x / 2, side_y / 2, side_z);
    glVertex3f(side_x / 2, -side_y / 2, side_z);

    glNormal3f(0, -1, 0);
    glVertex3f(-side_x / 2, side_y / 2, side_z);
    glVertex3f(-side_x / 2, side_y / 2, 0);
    glVertex3f(side_x / 2, side_y / 2, 0);
    glVertex3f(side_x / 2, side_y / 2, side_z);

    glNormal3f(0, -1, 0);
    glVertex3f(-side_x / 2, -side_y / 2, side_z);
    glVertex3f(-side_x / 2, -side_y / 2, 0);
    glVertex3f(side_x / 2, -side_y / 2, 0);
    glVertex3f(side_x / 2, -side_y / 2, side_z);

    glNormal3f(1, 0, 0);
    glVertex3f(side_x / 2, -side_y / 2, 0);
    glVertex3f(side_x / 2, side_y / 2, 0);
    glVertex3f(side_x / 2, side_y / 2, side_z);
    glVertex3f(side_x / 2, -side_y / 2, side_z);

    glNormal3f(-1, 0, 0);
    glVertex3f(-side_x / 2, -side_y / 2, 0);
    glVertex3f(-side_x / 2, side_y / 2, 0);
    glVertex3f(-side_x / 2, side_y / 2, side_z);
    glVertex3f(-side_x / 2, -side_y / 2, side_z);
    glEnd();
}

void RenderScene()
{
    glPushMatrix();
    glColor3f(1, 1, 1);
    glBegin(GL_TRIANGLE_STRIP);
    glNormal3f(0, 1, 0);
    glVertex3f(FLOOR_SIZE_SIDE, 0, FLOOR_SIZE_SIDE);
    glVertex3f(FLOOR_SIZE_SIDE, 0, -FLOOR_SIZE_SIDE);
    glVertex3f(-FLOOR_SIZE_SIDE, 0, FLOOR_SIZE_SIDE);
    glVertex3f(-FLOOR_SIZE_SIDE, 0, -FLOOR_SIZE_SIDE);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(5, 0.5, 5);
    glColor3f(1,0.2,0.2);
    Box(2, 1, 3);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 1, 0);
    glColor3f(0, 1, 0);
    glRotatef(90, 0, 1, 0);
    glutSolidTeapot(1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(2, 1, -2);
    glColor3f(1, 0, 0);
    glTranslatef(0, 0, 4);
    glRotatef(90, 0, 1, 0);
    glutWireCube(2);
    glPopMatrix();

    glPushMatrix();
    glColor3f(1,1,0);
    float ball_x = cosf(ball_alpha) * ball_radius;
    float ball_z = sinf(ball_alpha) * ball_radius;
    float ball_y = 1;
    glTranslatef(ball_x,ball_y, ball_z);
    glutSolidSphere(2, 360, 360);
    glPopMatrix();
}

void SetLights()
{
    glPushMatrix();
    glColor3f(0.5, 1, 0.5);
    glTranslatef(light0_x, light0_y, light0_z);
    glutSolidSphere(1, 360, 360);
    float position[] = {0, 0, 0, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    float ambient[] = {light0_ambient, light0_ambient, light0_ambient, 1};
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    float diffuse[] = {light0_diffuse, light0_diffuse, light0_diffuse, 1};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    float specular[] = {light0_specular, light0_specular, light0_specular, 1};
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glPopMatrix();
}

void SetCamera()
{
    float camera_x = cosf(cam_alpha) * cam_radius;
    float camera_z = sinf(cam_alpha) * cam_radius;

    gluLookAt(camera_x, cam_height, camera_z,
              0, 0, 0,
              0, 1, 0);
}

void Display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    SetCamera();
    SetLights();
    RenderScene();

    glutSwapBuffers();
}

void SetCameraVolum(int width, int height)
{
    glMatrixMode(GL_PROJECTION);

    if (CAMERA_PERSPECTIVE == cam_type)
        gluPerspective(60, (GLdouble) width / height, 1.0f, 100.0f);
    else if (CAMERA_ORTHOGONAL == cam_type)
        glOrtho(-ORTHO_VOLUME_SIDE / 2, ORTHO_VOLUME_SIDE / 2, -ORTHO_VOLUME_SIDE / 2, ORTHO_VOLUME_SIDE / 2, 1, 100);

    glMatrixMode(GL_MODELVIEW);
}

void SetViewportSize(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    glMatrixMode(GL_MODELVIEW);
}

void Reshape(int width, int height)
{
    SetViewportSize(width, height);
    SetCameraVolum(width, height);
    vpWidth = width;
    vpHeight = height;
}

void SpecialKeyboard(int key, int x, int y)
{

    switch (key)
    {
        case GLUT_KEY_UP:
            cam_height -= FPS * 10;
            cam_radius -= FPS * 10;
            break;
        case GLUT_KEY_DOWN:
            cam_height += FPS * 10;
            cam_radius += FPS * 10;
            break;
        case GLUT_KEY_RIGHT:
            cam_alpha += M_PI * FPS;
            break;
        case GLUT_KEY_LEFT:
            cam_alpha -= M_PI * FPS;
            break;
    }

    glutPostRedisplay();
}

void Keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'p':
            cam_type = (cam_type == CAMERA_ORTHOGONAL) ? CAMERA_PERSPECTIVE : CAMERA_ORTHOGONAL;
            Reshape(vpWidth, vpHeight);
            break;
        case 'a':
            light0_ambient -= FPS * 0.5;
            break;
        case 'A':
            light0_ambient += FPS * 0.5;
            break;
        case 's':
            light0_specular -= FPS * 0.5;
            break;
        case 'S':
            light0_specular += FPS * 0.5;
            break;
        case 'd':
            light0_diffuse -= FPS * 0.5;
            break;
        case 'D':
            light0_diffuse += FPS * 0.5;
            break;
        case 'z':
            light0_z -= FPS * 10;
            break;
        case 'Z':
            light0_z += FPS * 10;
            break;
        case 'x':
            light0_x -= FPS * 10;
            break;
        case 'X':
            light0_x += FPS * 10;
            break;
        case 'c':
            light0_y -= FPS * 10;
            break;
        case 'C':
            light0_y += FPS * 10;
            break;
        case 'f':
            glShadeModel(GL_FLAT);
            break;
        case 'F':
            glShadeModel(GL_SMOOTH);
            break;
    }

    glutPostRedisplay();
}

void WireEvents()
{
    glutKeyboardFunc(Keyboard);
    glutSpecialFunc(SpecialKeyboard);
    glutReshapeFunc(Reshape);
    glutDisplayFunc(Display);
    glutTimerFunc(FPS * 1000, BallRotator, 0);
}

void InitializeState()
{
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
}

void CreateWindow()
{
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(W_WIDTH, W_HEIGHT);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Mi primera Ventana");
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    CreateWindow();
    InitializeState();
    WireEvents();

    glutMainLoop();
    return 0;
}
