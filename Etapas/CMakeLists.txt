cmake_minimum_required(VERSION 3.3)
project(Etapas)

IF(CMAKE_SYSTEM_NAME STREQUAL Linux)
    set( CMAKE_CXX_FLAGS "-std=c++11 -lm -lGL -lGLU -lglut" )
ELSE()
    set(CMAKE_CXX_LINK_FLAGS "-Wc++11-extensions -std=c++11 -framework OpenGL -framework GLUT")
ENDIF()

file( GLOB SRCS ./*.cpp ./*.h)

add_executable( main ${SRCS} )
